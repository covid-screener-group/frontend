const { paramCase, snakeCase } = require('change-case');

/**
 * @return {String}
 */
module.exports = function () {
  // @ts-ignore
  const { fileName, fileType } = this;

  return `${fileType === 'py' ? snakeCase(fileName) : paramCase(fileName)}`;
};
